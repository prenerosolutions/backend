 <?php
 include "topbar.php";
?>
      <?php
        include "sidebar.php";
     ?>

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-code bg-c-blue"></i>
                        <div class="d-inline">
                            <h4>Add News </h4>
                            <span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                       <!---- <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Form Components</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Form Components</a>
                            </li>
                        </ul>  -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Basic Form Inputs card start -->
                    <div class="card">
                        <div class="card-header">
                           <!--- <h5>Basic Form Inputs</h5>
                            <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                            <div class="card-header-right"><i
                                class="icofont icofont-spinner-alt-5"></i></div>

                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>

                            </div>
                            <div class="card-block">
                               <!--- <h4 class="sub-title">Basic Inputs</h4> -->
                               
                               
                               <form id="event-form" method="post" action="news_process.php" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">News Title</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="title">
                                        </div>
                                    </div>
                                    
                                    
                                    
                                        
                                            
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Featured Image</label>
                                                    <div class="col-sm-10">
                                                        <input type="file" class="form-control" name="file">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Detail</label>
                                                    <div class="col-sm-10">
                                                        <textarea rows="5" cols="5" class="form-control"
                                                        placeholder="Default textarea" name="details"></textarea>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary float-right" id="primary-popover-content" data-container="body" data-toggle="popover" title="Primary color states" data-placement="bottom">Add News</button>


                                            </form>
                                         
                                        </div>
                                    </div>
                                    <!-- Basic Form Inputs card end -->
                                    
                                 
                                   
                                                   
                                                </div>
                                            </div>
                                            <!-- Page body end -->


                   <?php
                    include "footer.php";
                   ?>